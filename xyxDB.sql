##我的图书馆

##书籍分类表
create table book_tag(
	book_tag_id serial primary key,
	book_type varchar(255),
	create_date timestamp DEFAULT CURRENT_TIMESTAMP not null,   
	create_user varchar(255)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
##书本信息表
create table book(
	book_id serial primary key,
	book_tag_id bigint(20) unsigned not null,
	book_name varchar(255),
	intro varchar(255),
	page_size int,
	create_date timestamp DEFAULT CURRENT_TIMESTAMP not null,   
	create_user varchar(255)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
###书本每页内容表
create table book_content(
	book_content_id serial primary key,
	book_id bigint(20) unsigned not null,
	content text,
	remark varchar(255),
	page_index int,
	create_date timestamp DEFAULT CURRENT_TIMESTAMP not null,   
	create_user varchar(255)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
