package com.xyx.spring.Dao;

import java.util.List;

import com.xyx.spring.Model.User;
import com.xyx.spring.Utils.DataWrapper;

public interface UserDao {
	User getByUserName(String userName);
	User getById(Long id);
	boolean deleteUser(Long userId);
	boolean addUser(User user);
	boolean updateUser(User user);
	DataWrapper<List<User>> getUserList(Integer pageSize,Integer pageIndex,User user);
	DataWrapper<User> findUserLike(User user);
}
