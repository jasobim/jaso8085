package com.xyx.spring.xyxLibraries.Entity;
import java.util.Date;

import javax.persistence.*;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@Entity
@Table(name="book_content")
@ApiModel(description= "书本每页的内容表")
public class BookContent {
	@ApiModelProperty(value = "每页id")
	private Long bookContentId;
	@ApiModelProperty(value = "书本id")
	private Long bookId;
	@ApiModelProperty(value = "每页内容")
	private String content;
	@ApiModelProperty(value = "当前页码")
	private Integer pageIndex;
	@ApiModelProperty(value = "录入时间")
	private Date createDate;
	@ApiModelProperty(value = "录入人员")
	private String createUser;
	@ApiModelProperty(value = "当前页备注")
	private String remark;
	
	@Id
	@GeneratedValue
	@Column(name="book_content_id")
	public Long getBookContentId() {
		return bookContentId;
	}
	public void setBookContentId(Long bookContentId) {
		this.bookContentId = bookContentId;
	}
	
	@Basic
	@Column(name="book_id")
	public Long getBookId() {
		return bookId;
	}
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	
	@Basic
	@Column(name="content")
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	@Basic
	@Column(name="page_index")
	public Integer getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	
	@Basic
	@Column(name="create_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Basic
	@Column(name="create_user")
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	@Basic
	@Column(name="remark")
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
