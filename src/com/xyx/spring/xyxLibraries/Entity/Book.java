package com.xyx.spring.xyxLibraries.Entity;
import java.util.Date;

import javax.persistence.*;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@Entity
@Table(name="book")
@ApiModel(description= "书本信息表")
public class Book {
	@ApiModelProperty(value = "书本id")
	private Long bookId;
	@ApiModelProperty(value = "书籍分类id")
	private Long bookTagId;
	@ApiModelProperty(value = "书本名称")
	private String bookName;
	@ApiModelProperty(value = "书本简介")
	private String intro;
	@ApiModelProperty(value = "书本页数")
	private Integer pageSize;
	@ApiModelProperty(value = "创建时间")
	private Date createDate;
	@ApiModelProperty(value = "创建人")
	private String createUser;
	
	@Id
	@GeneratedValue
	@Column(name="book_id")
	public Long getBookId() {
		return bookId;
	}
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	
	@Basic
	@Column(name="book_tag_id")
	public Long getBookTagId() {
		return bookTagId;
	}
	public void setBookTagId(Long bookTagId) {
		this.bookTagId = bookTagId;
	}
	
	@Basic
	@Column(name="book_name")
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	
	@Basic
	@Column(name="intro")
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	
	@Basic
	@Column(name="page_size")
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
	@Basic
	@Column(name="create_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Basic
	@Column(name="create_user")
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	
}
