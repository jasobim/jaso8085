package com.xyx.spring.xyxLibraries.Entity;

import java.util.Date;
import javax.persistence.*;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@Entity
@Table(name="book_tag")
@ApiModel(description= "书籍标签分类表")
public class BookTag {
	@ApiModelProperty(value = "书籍分类id")
	private Long bookTagId;
	@ApiModelProperty(value = "书籍分类：A、金融 B、技术  C、诗集  D、哲理教育")
	private String bookType;
	@ApiModelProperty(value = "创建时间")
	private Date createDate;
	@ApiModelProperty(value = "创建人")
	private String createUser;
	
	@Id
	@GeneratedValue
	@Column(name="book_tag_id")
	public Long getBookTagId() {
		return bookTagId;
	}
	public void setBookTagId(Long bookTagId) {
		this.bookTagId = bookTagId;
	}
	
	@Basic
	@Column(name="book_type")
	public String getBookType() {
		return bookType;
	}
	public void setBookType(String bookType) {
		this.bookType = bookType;
	}
	
	@Basic
	@Column(name="create_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Basic
	@Column(name="create_user")
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	
	
	
}
