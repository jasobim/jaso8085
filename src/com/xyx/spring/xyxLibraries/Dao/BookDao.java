package com.xyx.spring.xyxLibraries.Dao;

import java.util.List;

import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.xyxLibraries.Entity.Book;

public interface BookDao {
	boolean addBook(Book book);
	boolean deleteBook(Long bookId);
	Book getBookById(Long bookId);
	DataWrapper<List<Book>> getBookList(Book book,Integer pageIndex,Integer pageSize);
}
