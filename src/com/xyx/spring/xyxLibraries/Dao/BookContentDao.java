package com.xyx.spring.xyxLibraries.Dao;

import java.util.List;

import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.xyxLibraries.Entity.BookContent;

public interface BookContentDao {
	boolean addBookContent(BookContent bookContent);
	boolean deleteBookContent(Long bookContentId);
	boolean updateBookContent(BookContent bookContent);
	DataWrapper<List<BookContent>> getBookContentList(BookContent bookContent,Integer pageIndex,Integer pageSize);
	BookContent getById(Long bookContentId);
}
