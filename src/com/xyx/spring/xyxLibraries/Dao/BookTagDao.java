package com.xyx.spring.xyxLibraries.Dao;

import java.util.List;
import com.xyx.spring.xyxLibraries.Entity.BookTag;

public interface BookTagDao {
	boolean addBookTag(BookTag bookTag);
	boolean deleteBookTag(Long bookTagId);
	BookTag getBookTagById(Long bookTagId);
	List<BookTag> getBookTagList();
}
