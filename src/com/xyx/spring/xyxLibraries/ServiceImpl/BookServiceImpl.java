package com.xyx.spring.xyxLibraries.ServiceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xyx.spring.Enums.ErrorCodeEnum;
import com.xyx.spring.Model.User;
import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.Utils.SessionManager;
import com.xyx.spring.xyxLibraries.Dao.BookDao;
import com.xyx.spring.xyxLibraries.Entity.Book;
import com.xyx.spring.xyxLibraries.Service.BookService;
@Service("bookService")
public class BookServiceImpl implements BookService {
	@Autowired
	BookDao bookDao;
	@Override
	public DataWrapper<Boolean> addBook(Book book, String token) {
		// TODO Auto-generated method stub
		DataWrapper<Boolean> result = new DataWrapper<Boolean>();
		User userInMemory = SessionManager.getSession(token);
		if(userInMemory!=null){
			if(book!=null){
				book.setCreateDate(new Date());
				if(book.getCreateUser()==null){
					book.setCreateUser(userInMemory.getRealName());
				}
				result.setData(bookDao.addBook(book));
			}else{
				result.setErrorCode(ErrorCodeEnum.Empty_Inputs);
			}
		}else{
			result.setErrorCode(ErrorCodeEnum.User_Not_Logined);
		}
		return result;
	}

	@Override
	public DataWrapper<Boolean> deleteBook(Long bookId, String token) {
		// TODO Auto-generated method stub
		DataWrapper<Boolean> result = new DataWrapper<Boolean>();
		User userInMemory = SessionManager.getSession(token);
		if(userInMemory!=null){
			result.setData(bookDao.deleteBook(bookId));
		}else{
			result.setErrorCode(ErrorCodeEnum.User_Not_Logined);
		}
		return result;
	}

	@Override
	public DataWrapper<List<Book>> getBookList(Book book,Integer pageIndex,Integer pageSize, String token) {
		// TODO Auto-generated method stub
		DataWrapper<List<Book>> result = new DataWrapper<List<Book>>();
		User userInMemory = SessionManager.getSession(token);
		if(userInMemory!=null){
			result = bookDao.getBookList(book,pageIndex,pageSize);
		}else{
			result.setErrorCode(ErrorCodeEnum.User_Not_Logined);
		}
		return result;
	}

}
