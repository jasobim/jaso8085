package com.xyx.spring.xyxLibraries.ServiceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xyx.spring.Enums.ErrorCodeEnum;
import com.xyx.spring.Model.User;
import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.Utils.SessionManager;
import com.xyx.spring.xyxLibraries.Dao.BookTagDao;
import com.xyx.spring.xyxLibraries.Entity.BookTag;
import com.xyx.spring.xyxLibraries.Service.BookTagService;
@Service("bookTagService")
public class BookTagServiceImpl implements BookTagService {
	@Autowired
	BookTagDao bookTagDao;
	@Override
	public DataWrapper<Boolean> deleteBookTag(Long bookTagId, String token) {
		// TODO Auto-generated method stub
		DataWrapper<Boolean> result = new DataWrapper<Boolean>();
		User userInMemory = SessionManager.getSession(token);
		if(userInMemory!=null){
			result.setData(bookTagDao.deleteBookTag(bookTagId));
		}else{
			result.setErrorCode(ErrorCodeEnum.User_Not_Logined);
		}
		return result;
	}

	@Override
	public DataWrapper<BookTag> getById(Long id,String token) {
		// TODO Auto-generated method stub
		DataWrapper<BookTag> result = new DataWrapper<BookTag>();
		User userInMemory = SessionManager.getSession(token);
		if(userInMemory!=null){
			result.setData(bookTagDao.getBookTagById(id));
		}else{
			result.setErrorCode(ErrorCodeEnum.User_Not_Logined);
		}
		return result;
	}

	@Override
	public DataWrapper<Boolean> addBookTag(BookTag bookTag,String token) {
		// TODO Auto-generated method stub
		DataWrapper<Boolean> result = new DataWrapper<Boolean>();
		User userInMemory = SessionManager.getSession(token);
		if(userInMemory!=null){
			if(bookTag!=null){
				bookTag.setCreateDate(new Date());
				if(bookTag.getCreateUser()==null){
					bookTag.setCreateUser(userInMemory.getRealName());
				}
				result.setData(bookTagDao.addBookTag(bookTag));
			}else{
				result.setErrorCode(ErrorCodeEnum.Empty_Inputs);
			}
		}else{
			result.setErrorCode(ErrorCodeEnum.User_Not_Logined);
		}
		return result;
	}

	@Override
	public DataWrapper<List<BookTag>> getBookTagList(String token) {
		// TODO Auto-generated method stub
		DataWrapper<List<BookTag>> result = new DataWrapper<List<BookTag>>();
		User userInMemory = SessionManager.getSession(token);
		if(userInMemory!=null){
			result.setData(bookTagDao.getBookTagList());
		}else{
			result.setErrorCode(ErrorCodeEnum.User_Not_Logined);
		}
		return result;
	}

}
