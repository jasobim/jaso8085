package com.xyx.spring.xyxLibraries.ServiceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xyx.spring.Enums.ErrorCodeEnum;
import com.xyx.spring.Model.User;
import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.Utils.SessionManager;
import com.xyx.spring.xyxLibraries.Dao.BookContentDao;
import com.xyx.spring.xyxLibraries.Entity.BookContent;
import com.xyx.spring.xyxLibraries.Service.BookContentService;
@Service("bookContentService")
public class BookContentServiceImpl implements BookContentService {
	@Autowired
	BookContentDao bookContentDao;
	@Override
	public DataWrapper<Boolean> addBookContent(BookContent bookContent, String token) {
		// TODO Auto-generated method stub
		DataWrapper<Boolean> result = new DataWrapper<Boolean>();
		User userInMemory = SessionManager.getSession(token);
		if(userInMemory!=null){
			if(bookContent!=null){
				if(bookContent.getCreateUser()==null){
					bookContent.setCreateUser(userInMemory.getRealName());
				}
				bookContent.setCreateDate(new Date());
				result.setData(bookContentDao.addBookContent(bookContent));
			}else{
				result.setErrorCode(ErrorCodeEnum.Empty_Inputs);
			}
		}else{
			result.setErrorCode(ErrorCodeEnum.User_Not_Logined);
		}
		return result;
	}

	@Override
	public DataWrapper<Boolean> deleteBookContent(Long bookContentId, String token) {
		// TODO Auto-generated method stub
		DataWrapper<Boolean> result = new DataWrapper<Boolean>();
		User userInMemory = SessionManager.getSession(token);
		if(userInMemory!=null){
			if(bookContentId!=null){
				result.setData(bookContentDao.deleteBookContent(bookContentId));
			}else{
				result.setErrorCode(ErrorCodeEnum.Empty_Inputs);
			}
		}else{
			result.setErrorCode(ErrorCodeEnum.User_Not_Logined);
		}
		return result;
	}

	@Override
	public DataWrapper<Boolean> updateBookContent(BookContent bookContent, String token) {
		// TODO Auto-generated method stub
		DataWrapper<Boolean> result = new DataWrapper<Boolean>();
		User userInMemory = SessionManager.getSession(token);
		if(userInMemory!=null){
			if(bookContent.getBookContentId()!=null){
				BookContent old = bookContentDao.getById(bookContent.getBookContentId());
				if(old!=null){
					if(bookContent.getContent()!=null){
						old.setContent(bookContent.getContent());
					}
					if(bookContent.getRemark()!=null){
						old.setRemark(bookContent.getRemark());
					}
					result.setData(bookContentDao.updateBookContent(old));
				}
			}
		}else{
			result.setErrorCode(ErrorCodeEnum.User_Not_Logined);
		}
		return result;
	}

	@Override
	public DataWrapper<List<BookContent>> getBookContentList(BookContent bookContent, Integer pageIndex,
			Integer pageSize, String token) {
		// TODO Auto-generated method stub
		DataWrapper<List<BookContent>> result = new DataWrapper<List<BookContent>>();
		User userInMemory = SessionManager.getSession(token);
		if(userInMemory!=null){
			result = bookContentDao.getBookContentList(bookContent, pageIndex, pageSize);
		}else{
			result.setErrorCode(ErrorCodeEnum.User_Not_Logined);
		}
		return result;
	}

}
