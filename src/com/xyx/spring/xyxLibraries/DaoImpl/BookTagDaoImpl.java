package com.xyx.spring.xyxLibraries.DaoImpl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.xyx.spring.Dao.BaseDao;
import com.xyx.spring.xyxLibraries.Dao.BookTagDao;
import com.xyx.spring.xyxLibraries.Entity.BookTag;
@Repository
public class BookTagDaoImpl extends BaseDao<BookTag> implements BookTagDao {

	@Override
	public boolean addBookTag(BookTag bookTag) {
		// TODO Auto-generated method stub
		return save(bookTag);
	}

	@Override
	public boolean deleteBookTag(Long bookTagId) {
		// TODO Auto-generated method stub
		return delete(get(bookTagId));
	}

	@Override
	public BookTag getBookTagById(Long bookTagId) {
		// TODO Auto-generated method stub
		return get(bookTagId);
	}

	@Override
	public List<BookTag> getBookTagList() {
		// TODO Auto-generated method stub
		List<BookTag> resultList = new ArrayList<BookTag>();
		Session session = getSession();
        Criteria criteria = session.createCriteria(BookTag.class);
        try {
        	resultList = criteria.list();
        }catch (Exception e){
            e.printStackTrace();
        }
		return resultList;
	}

}
