package com.xyx.spring.xyxLibraries.DaoImpl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.xyx.spring.Dao.BaseDao;
import com.xyx.spring.Utils.DaoUtil;
import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.xyxLibraries.Dao.BookContentDao;
import com.xyx.spring.xyxLibraries.Entity.BookContent;
@Repository
public class BookContentDaoImpl extends BaseDao<BookContent> implements BookContentDao {

	@Override
	public boolean addBookContent(BookContent bookContent) {
		// TODO Auto-generated method stub
		return save(bookContent);
	}

	@Override
	public boolean deleteBookContent(Long bookContentId) {
		// TODO Auto-generated method stub
		return delete(get(bookContentId));
	}

	@Override
	public boolean updateBookContent(BookContent bookContent) {
		// TODO Auto-generated method stub
		return update(bookContent);
	}

	@Override
	public DataWrapper<List<BookContent>> getBookContentList(BookContent bookContent, Integer pageIndex,
			Integer pageSize) {
		DataWrapper<List<BookContent>> result = new DataWrapper<List<BookContent>>();
		List<BookContent> ret = new ArrayList<BookContent>();
		Session session = getSession();
		Criteria criteria = session.createCriteria(BookContent.class);
		if(bookContent.getBookId()!=null){
			criteria.add(Restrictions.eq("bookId", bookContent.getBookId()));
		}
		if(bookContent.getContent()!=null){
			criteria.add(Restrictions.like("content","%"+ bookContent.getContent()+"%"));
		}
		if(bookContent.getRemark()!=null){
			criteria.add(Restrictions.like("remark","%"+ bookContent.getRemark()+"%"));
		}
		if (pageSize == null) {
			pageSize = 10;
		}
        if (pageIndex == null) {
        	pageIndex = 0;
		}
        // 取总页数
        criteria.setProjection(Projections.rowCount());
        int totalItemNum = ((Long)criteria.uniqueResult()).intValue();
        int totalPageNum = DaoUtil.getTotalPageNumber(totalItemNum, pageSize);

        // 真正取值
        criteria.setProjection(null);
        if (pageSize > 0 && pageIndex > 0) {
            criteria.setMaxResults(pageSize);// 最大显示记录数
            criteria.setFirstResult((pageIndex - 1) * pageSize);// 从第几条开始
        }
        try {
            ret = criteria.list();
        }catch (Exception e){
            e.printStackTrace();
        }
        result.setData(ret);
        result.setTotalNumber(totalItemNum);
        result.setCurrentPage(pageIndex);
        result.setTotalPage(totalPageNum);
        result.setNumberPerPage(pageSize);

        return result;
	}

	@Override
	public BookContent getById(Long bookContentId) {
		// TODO Auto-generated method stub
		return get(bookContentId);
	}

}
