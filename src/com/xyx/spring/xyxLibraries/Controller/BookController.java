package com.xyx.spring.xyxLibraries.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.xyxLibraries.Entity.Book;
import com.xyx.spring.xyxLibraries.Service.BookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Api(value="书本controller",tags="书本操作接口")
@Controller
@RequestMapping(value="api/book")
public class BookController {
	@Autowired
	BookService bookService;
	/**
	 * @param bookTagId   //必须
	 * @return
	 */
	@ApiOperation(value="书本添加接口",notes="添加")
	@RequestMapping(value="/addBook", method = RequestMethod.POST)
    @ResponseBody
	public DataWrapper<Boolean> addBook(
			@ModelAttribute Book book,
			@RequestParam(value="token",required=true) String token){
		return bookService.addBook(book, token);
	}
	
	/**
	 * @param bookId   //必须
	 * @return
	 */
	@ApiOperation(value="书本删除接口",notes="删除")
	@RequestMapping(value="/deleteBook", method = RequestMethod.GET)
    @ResponseBody
	public DataWrapper<Boolean> deleteBook(
			@RequestParam(value="bookId",required=true) Long bookId,
			@RequestParam(value="token",required=true) String token){
		return bookService.deleteBook(bookId, token);
	}
	
	/**
	 * @param bookTagId   //必须
	 * @return
	 */
	@ApiOperation(value="书本列表获取接口",notes="列表获取")
	@ApiImplicitParams({
		  @ApiImplicitParam(name="pageIndex",value="当前页码",dataType="int", paramType = "query"),
		  @ApiImplicitParam(name="pageSize",value="每页条数",dataType="int", paramType = "query")})
	@RequestMapping(value="/getBookList", method = RequestMethod.GET)
    @ResponseBody
	public DataWrapper<List<Book>> getBookList(
			@ModelAttribute Book book,
			@RequestParam(value="pageIndex",required=false) Integer pageIndex,
			@RequestParam(value="pageSize",required=false) Integer pageSize,
			@RequestParam(value="token",required=true) String token){
		return bookService.getBookList(book, pageIndex, pageSize, token);
	}
	
	
}
