package com.xyx.spring.xyxLibraries.Controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.xyxLibraries.Entity.BookContent;
import com.xyx.spring.xyxLibraries.Service.BookContentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
@Api(value="书本内容controller",tags="书本每页内容操作接口")
@Controller
@RequestMapping(value="api/bookContent")
public class BookContentController {
	@Autowired
	BookContentService bookContentService;
	/**
	 * @param bookId   //必须
	 * @return
	 */
	@ApiOperation(value="书本内容录入接口",notes="录入")
	@RequestMapping(value="/addBookContent", method = RequestMethod.POST)
    @ResponseBody
	public DataWrapper<Boolean> addBookContent(
			@ModelAttribute BookContent book,
			@RequestParam(value="token",required=true) String token){
		return bookContentService.addBookContent(book, token);
	}
	
	/**
	 * @param bookId   //必须
	 * @return
	 */
	@ApiOperation(value="书本内容删除接口",notes="删除")
	@RequestMapping(value="/deleteBookContent", method = RequestMethod.GET)
    @ResponseBody
	public DataWrapper<Boolean> deleteBookContent(
			@RequestParam(value="bookContentId",required=true) Long bookContentId,
			@RequestParam(value="token",required=true) String token){
		return bookContentService.deleteBookContent(bookContentId, token);
	}
	
	/**
	 * @param bookTagId   //必须
	 * @return
	 */
	@ApiOperation(value="书本内容列表获取接口",notes="列表获取")
	@ApiImplicitParams({
		  @ApiImplicitParam(name="pageIndex",value="当前页码",dataType="int", paramType = "query"),
		  @ApiImplicitParam(name="pageSize",value="每页条数",dataType="int", paramType = "query")})
	@RequestMapping(value="/getBookContentList", method = RequestMethod.GET)
    @ResponseBody
	public DataWrapper<List<BookContent>> getBookContentList(
			@ModelAttribute BookContent bookContent,
			@RequestParam(value="pageIndex",required=false) Integer pageIndex,
			@RequestParam(value="pageSize",required=false) Integer pageSize,
			@RequestParam(value="token",required=true) String token){
		return bookContentService.getBookContentList(bookContent, pageIndex, pageSize, token);
	}
}
