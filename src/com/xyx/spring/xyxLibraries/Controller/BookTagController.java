package com.xyx.spring.xyxLibraries.Controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.xyxLibraries.Entity.BookTag;
import com.xyx.spring.xyxLibraries.Service.BookTagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@Api(value="书籍分类controller", tags="书籍分类接口")
@Controller
@RequestMapping(value="api/bookTag")
public class BookTagController {
	@Autowired
	BookTagService bookTagService;
	/**
	 * @param bookType   //必须
	 * @param createUser 可选,没有的话默认当前用户
	 * 其他参数不需要，由程序指定，如日期
	 * @return
	 */
	@ApiOperation(value="书籍分类添加接口",notes="添加")
	@RequestMapping(value="/addBookTag", method = RequestMethod.POST)
    @ResponseBody
	public DataWrapper<Boolean> addBookTag(
			@ModelAttribute BookTag bookTag,
			@RequestParam(value="token",required=true) String token){
		return bookTagService.addBookTag(bookTag, token);
	}
	
	/**
	 * @param bookTagId   //必须
	 * @return
	 */
	@ApiOperation(value="书籍分类删除接口",notes="删除")
	@RequestMapping(value="/deleteBookTag", method = RequestMethod.GET)
    @ResponseBody
	public DataWrapper<Boolean> deleteBookTag(
			@RequestParam(value="bookTagId",required=true) Long bookTagId,
			@RequestParam(value="token",required=true) String token){
		return bookTagService.deleteBookTag(bookTagId, token);
	}
	
	/**
	 * @param bookTagId   //必须
	 * @return
	 */
	@ApiOperation(value="书籍分类查找接口",notes="查找")
	@RequestMapping(value="/getBookTagById", method = RequestMethod.GET)
    @ResponseBody
	public DataWrapper<BookTag> getBookTagById(
			@RequestParam(value="bookTagId",required=true) Long bookTagId,
			@RequestParam(value="token",required=true) String token){
		return bookTagService.getById(bookTagId, token);
	}
	
	/**
	 * @param bookTagId   //必须
	 * @return
	 */
	@ApiOperation(value="书籍分类列表获取接口",notes="列表获取")
	@RequestMapping(value="/getBookTagList", method = RequestMethod.GET)
    @ResponseBody
	public DataWrapper<List<BookTag>> getBookTagList(
			@RequestParam(value="token",required=true) String token){
		return bookTagService.getBookTagList(token);
	}
	
}
