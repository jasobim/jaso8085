package com.xyx.spring.xyxLibraries.Service;

import java.util.List;
import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.xyxLibraries.Entity.BookTag;

public interface BookTagService {
	DataWrapper<Boolean> deleteBookTag(Long bookTagId,String token);
	DataWrapper<BookTag> getById(Long id,String token);
	DataWrapper<Boolean> addBookTag(BookTag bookTag,String token);
    DataWrapper<List<BookTag>> getBookTagList(String token);
}
