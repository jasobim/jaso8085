package com.xyx.spring.xyxLibraries.Service;

import java.util.List;

import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.xyxLibraries.Entity.BookContent;

public interface BookContentService {
	DataWrapper<Boolean> addBookContent(BookContent bookContent,String token);
	DataWrapper<Boolean> deleteBookContent(Long BookContentId,String token);
	DataWrapper<Boolean> updateBookContent(BookContent bookContent,String token);
	DataWrapper<List<BookContent>> getBookContentList(BookContent bookContent,Integer pageIndex,Integer pageSize,String token);
}
