package com.xyx.spring.xyxLibraries.Service;

import java.util.List;

import com.xyx.spring.Utils.DataWrapper;
import com.xyx.spring.xyxLibraries.Entity.Book;

public interface BookService {
	DataWrapper<Boolean> addBook(Book book,String token);
	DataWrapper<Boolean> deleteBook(Long bookId,String token);
	DataWrapper<List<Book>> getBookList(Book book,Integer pageIndex,Integer pageSize,String token);
}
