package com.xyx.spring.Service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.xyx.spring.Model.Files;
import com.xyx.spring.Utils.DataWrapper;

public interface FileService {
	boolean deleteFileByPath(String filePathAndName,HttpServletRequest request);
	Files getById(Long id);
	boolean deleteFileById(Long id);
	boolean addFile(Files file);
    DataWrapper<List<Files>> getAllFiles(String token,String fileName);
	DataWrapper<List<String>> uploadFile(String filePath, MultipartFile[] file, Integer fileType,HttpServletRequest request,String token,Integer exchange);
	Files uploadFiles(String filePath, MultipartFile file, Integer fileType,HttpServletRequest request);
	DataWrapper<String> changeFiles(MultipartFile newFile, Integer filetype,
			HttpServletRequest request, String token, String oldFileUrl);
}
