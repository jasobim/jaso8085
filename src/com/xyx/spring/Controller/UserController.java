package com.xyx.spring.Controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.xyx.spring.Enums.UserTypeEnum;
import com.xyx.spring.Model.User;
import com.xyx.spring.Model.Pojo.UserPojo;
import com.xyx.spring.Service.UserService;
import com.xyx.spring.Utils.DataWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

//将查询到的数据缓存到myCache中,并使用方法名称加上参数中的userNo作为缓存的key    
//通常更新操作只需刷新缓存中的某个值,所以为了准确的清除特定的缓存,故定义了这个唯一的key,从而不会影响其它缓存值    
//@Cacheable(value="myCache", key="#id")   
@Api(value="用户controller",tags="用户操作接口")
@Controller
@RequestMapping(value="api/user")
public class UserController {
	@Autowired
	UserService userService;
	
	/**
	 * 
	 * @param userName、password、realName   //必须
	 * @param email、tel可有可无
	 * 其他参数不需要，由程序指定，如日期，用户类型
	 * @return
	 */

	@ApiOperation(value="用户注册接口",notes="注册")
	@RequestMapping(value="/register", method = RequestMethod.POST)
    @ResponseBody
    public DataWrapper<Void> register(
    		@ModelAttribute User user) {
        return userService.register(user);
    }
	@ApiOperation(value="用户添加接口",notes="登陆后管理员添加接口")
	@ApiImplicitParams({
		  @ApiImplicitParam(name="name",value="用户名",dataType="string", paramType = "query",defaultValue="textss"),
		  @ApiImplicitParam(name="token",value="登录凭证",dataType="String", paramType = "query")})
	@RequestMapping(value="/admin/addUser", method = RequestMethod.POST)
    @ResponseBody
    public DataWrapper<Void> addUser(
    		@ModelAttribute User user,
    		HttpServletRequest request,
    		@RequestParam(value = "file", required = false) MultipartFile file,
    		@RequestParam(value="token",required=true) String token) {
         return userService.addUser(user,token,file,request);
    }
	
	@ApiOperation(value="用户登录接口",notes="输入用户名和密码进行登录")
	@ApiImplicitParams({
		  @ApiImplicitParam(name="username",value="用户名",dataType="string", paramType = "query"),
		  @ApiImplicitParam(name="password",value="密码",dataType="String", paramType = "query")})
	@RequestMapping(value="/login", method = RequestMethod.POST)
    @ResponseBody
    public DataWrapper<UserPojo> Login(
    		@RequestParam(value="username",required=true) String username,
    		@RequestParam(value="password",required=true) String password) {
		DataWrapper<UserPojo> test=userService.login(username, password);
		return test;
    }
	@ApiOperation(value="密码找回接口",notes="通过用户名、真实姓名、联系方式和邮箱找回")
	@ApiImplicitParams(
			{
				@ApiImplicitParam(name="userName",value="用户名",dataType="string",paramType="query"),
				@ApiImplicitParam(name="realName",value="真实姓名",dataType="string",paramType="query"),
				@ApiImplicitParam(name="tel",value="联系方式",dataType="string",paramType="query"),
				@ApiImplicitParam(name="email",value="邮箱",dataType="string",paramType="query")
		})
	@RequestMapping(value="/findPass", method = RequestMethod.GET)
    @ResponseBody
    public DataWrapper<User> FindPs(
    		@ModelAttribute User user) {
		DataWrapper<User> test=userService.FindPs(user);
		return test;
    }
	/**
	 * 
	 * @param password///密码修改
	 * @param token
	 * @return
	 */
	@ApiOperation(value="修改自己密码接口",notes="输入旧密码，确认旧密码，更换新密码")
	@ApiImplicitParams({
		@ApiImplicitParam(name="oldPs",value="旧密码",dataType="string",paramType="query"),
		@ApiImplicitParam(name="newPs",value="新密码",dataType="string",paramType="query"),
		@ApiImplicitParam(name="token",value="登陆凭证",dataType="string",paramType="query")
	})
	@RequestMapping(value="/updatePsBySelf", method = RequestMethod.POST)
    @ResponseBody
    public DataWrapper<Void> UpdatePsBySelf(
    		@RequestParam(value="oldPs",required=true) String oldPs,
    		@RequestParam(value="newPs",required=true) String newPs,
    		@RequestParam(value="token",required=true) String token) {
        return userService.updateUserBySelf(oldPs, newPs, token);
    }
	
	
	//管理员获取其他用户的个人详情
	@ApiOperation(value = "通过ID查询USER信息", httpMethod = "GET", notes = "暂无")
	@ApiImplicitParams({
		@ApiImplicitParam(name="userId",value="用户id",dataType="long",paramType="query"),
		@ApiImplicitParam(name="token",value="登录凭证",dataType="string",paramType="query")
	})
    @ResponseBody
	@RequestMapping(value="/admin/getUserDetails", method = RequestMethod.GET)
    public DataWrapper<UserPojo> getUserDetailsByAdmin(
    		@RequestParam(value="userId",required=true) Long userId,
    		@RequestParam(value="token",required=true) String token) {
        return userService.getUserDetailsByAdmin(userId,token);
    }
	//管理员删除用户的个人信息
	@ApiOperation(value = "通过ID删除USER", httpMethod = "GET", notes = "暂无")
	@ApiImplicitParams({
		@ApiImplicitParam(name="userId",value="用户id",dataType="long",paramType="query"),
		@ApiImplicitParam(name="token",value="登录凭证",dataType="string",paramType="query")
	})
	@RequestMapping(value="/admin/deleteUser", method = RequestMethod.GET)
    @ResponseBody
    public DataWrapper<Void> deleteUserByAdmin(
    		@RequestParam(value="userId",required=true) Long userId,
	    	@RequestParam(value="token",required=true) String token) {
	        return userService.deleteUser(userId,token);
    }
	
	//管理员获取用户列表
	@ApiOperation(value = "获取用户列表", httpMethod = "GET", notes = "分页、可筛选查询")
	@ApiImplicitParams({
		  @ApiImplicitParam(name="userName",value="筛选条件（用户名）",dataType="string", paramType = "query"),
		  @ApiImplicitParam(name="token",value="登录凭证",dataType="String", paramType = "query"),
		  @ApiImplicitParam(name="realName",value="筛选条件（真实姓名）",dataType="string", paramType = "query"),
		  @ApiImplicitParam(name="pageIndex",value="当前页码",dataType="int", paramType = "query"),
		  @ApiImplicitParam(name="pageSize",value="每页条数",dataType="int", paramType = "query")})
	@RequestMapping(value="/admin/getUserList", method = RequestMethod.GET)
    @ResponseBody
    public DataWrapper<List<UserPojo>> getUserListByAdmin(
    		@RequestParam(value="pageIndex",required=false) Integer pageIndex,
    		@RequestParam(value="pageSize",required=false) Integer pageSize,
    		@ModelAttribute User user,
    		@RequestParam(value="token",required=true) String token) {
        return userService.getUserList(pageIndex,pageSize,user,token);
    }
	
	//修改用户权限
	@ApiOperation(value="修改用户权限",notes="通过用户id修改用户权限")
	@ApiImplicitParams({
		@ApiImplicitParam(name="userId",value="用户id",dataType="long",paramType="query"),
		@ApiImplicitParam(name="userType",value="用户角色",dataType="int",paramType="query"),
		@ApiImplicitParam(name="token",value="登录凭证",dataType="long",paramType="query")
	})
	@RequestMapping(value="/admin/changeUser/{userId}/type/{userType}", method = RequestMethod.POST)
    @ResponseBody
    public DataWrapper<Void> changeUserTypeByAdmin(
    		@PathVariable(value="userId") Long userId,
    		@PathVariable(value="userType") Integer userType,
    		@RequestParam(value="token",required=true) String token) {
		if (userType != 0 && userType != 1) {
			userType = UserTypeEnum.User.getType();
		}
        return userService.changeUserTypeByAdmin(userId,userType,token);
    }
	
	@ApiOperation(value="管理员修改用户信息接口",notes="管理员角色")
	@RequestMapping(value="/admin/updateUser", method = RequestMethod.POST)
    @ResponseBody
    public DataWrapper<Void> changeUserTypeByAdmin(
    		@ModelAttribute User user,
    		HttpServletRequest request,
    		@RequestParam(value = "file", required = false) MultipartFile file,
    		@RequestParam(value="token",required=true) String token) {
   
		return userService.updateUserByAdmin(user, token,file,request);
    }
}
