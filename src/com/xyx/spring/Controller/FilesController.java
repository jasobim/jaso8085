package com.xyx.spring.Controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.xyx.spring.Model.Files;
import com.xyx.spring.Service.FileService;
import com.xyx.spring.Utils.DataWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

//将查询到的数据缓存到myCache中,并使用方法名称加上参数中的userNo作为缓存的key    
//通常更新操作只需刷新缓存中的某个值,所以为了准确的清除特定的缓存,故定义了这个唯一的key,从而不会影响其它缓存值    
//@Cacheable(value="myCache", key="#id")   
//@CrossOrigin(origins = {"http://jasobim.com:8088"})
@Api(value="文件controller",tags="文件操作接口")
@Controller
@RequestMapping(value="api/files")
public class FilesController {
	@Autowired
	FileService fileService;
	private final static Integer fileType=0;//一般文件
	private final static String filePath="/uploadFiles/projectfiles";
	/**
	 * 文件上传接口
	 * @param userName、password、realName   //必须
	 * @param email、tel可有可无
	 * 其他参数不需要，由程序指定，如日期，用户类型
	 * @return
	 */

	@ApiOperation(value="文件上传接口",notes="上传")
	@RequestMapping(value="/uploadFiles", method = RequestMethod.POST)
    @ResponseBody
    public DataWrapper<List<String>> uploadFiles(
    		@RequestParam(value = "token", required = false) String token,
    		@RequestParam(value = "exchange", required = false) Integer exchange,//0、不转 （默认值）1、转png
    		@RequestParam(value = "file", required = false) MultipartFile[] file,
    		HttpServletRequest request) {
        return fileService.uploadFile(filePath,file,fileType, request,token,exchange);
    }
	
	
	//管理员获取用户列表
	@ApiOperation(value = "获取文件列表", httpMethod = "GET", notes = "可筛选查询")
	@ApiImplicitParams({
		  @ApiImplicitParam(name="fileName",value="筛选条件（文件名）",dataType="string", paramType = "query"),
		  @ApiImplicitParam(name="token",value="登录凭证",dataType="String", paramType = "query")})
	@RequestMapping(value="/getFilesList", method = RequestMethod.GET)
    @ResponseBody
    public DataWrapper<List<Files>> getFilesList(
    		@RequestParam(value="fileName",required=false) String fileName,
    		@RequestParam(value="token",required=true) String token) {
        return fileService.getAllFiles(token,fileName);
    }
	/**
	 *txt文件替换接口 
	 * 
	 */
	@ApiOperation(value="文件替换接口",notes="替换")
	@RequestMapping(value="/changeFiles", method = RequestMethod.POST)
    @ResponseBody
    public DataWrapper<String> changeFiles(
    		@RequestParam(value = "oldFileUrl", required = true) String oldFileUrl,
    		@RequestParam(value = "token", required = true) String token,
    		@RequestParam(value = "newFile", required = true) MultipartFile newFile,
    		HttpServletRequest request) {
        return fileService.changeFiles(newFile,fileType, request,token,oldFileUrl);
    }
}
