package com.xyx.spring.study.serializable;

import java.io.Serializable;

import lombok.Data;
@Data
public class PersonHession implements Serializable {
    private Long id;
    private String name;
    private Boolean male;
}