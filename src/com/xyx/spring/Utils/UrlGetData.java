package com.xyx.spring.Utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class UrlGetData {
	//url抓取数据（参数URL：就是你要抓数据的地址。如：http://www.cnev.cn/）
	public static String urlClimb(String url) throws Exception{
	    URL getUrl =new URL(url); //创建URl连接
	    HttpURLConnection connection = (HttpURLConnection) getUrl.openConnection(); //建立连接
	    connection.connect(); //打开连接
	    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8")); //创建输入流并设置编码
	    StringBuffer sb = new StringBuffer();
	    String lines = null;
	    while ((lines = reader.readLine()) != null) { 
	        lines = new String(lines.getBytes(), "utf-8"); //读取流的一行,设置编码
	        sb = sb.append(lines + "\n");
	    }
	    reader.close(); //关闭流
	    connection.disconnect(); //销毁连接
	    return sb.toString(); //返回抓取的数据(注意,这里是抓取了访问的网站的全部数据)
	}
	public static void main(String[] arg){
		try {
			String datas=urlClimb("https://yq.aliyun.com/articles/594217");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
